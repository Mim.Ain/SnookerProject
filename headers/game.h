//define class gam

#include "ball.h"

class Game
{
public:
    Game(int width, int height, int hRad);
    void move();
    int get_width();
    int get_height();
    int get_scores(int player);
    Ball *get_ball();

private:
    int width, height, scores[2], hRad;
    Ball *balls = new Ball[17];
};