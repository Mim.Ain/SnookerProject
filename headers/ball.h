//define class ball

#include <bits/stdc++.h>

class Ball
{
public:
    Ball(float x, float y, float dx, float dy, float speed);
    float get_score();
    float get_x();
    float get_y();
    void set_x(float x);
    void set_y(float y);
    void reverse_dy();
    void reverse_dx();
    void move();
private:
    float x, y, dx, dy, speed, score;
};
