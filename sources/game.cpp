#include "../headers/game.h"
#include<bits/stdc++.h>

Game::Game(int width, int height, int hRad) {
    this->width = width;
    this->height = height;
    scores[0] = scores[1] = 0;
    Ball **balls = new Ball *[17];
    int i = 0;
    balls[++i] = new Ball(770, 285, pow(2, 0.5), pow(2, 0.5), 5, 1);
    balls[++i] = new Ball(800, 260, pow(2, 0.5), pow(2, 0.5), 5, 1);
    balls[++i] = new Ball(800, 290, pow(2, 0.5), pow(2, 0.5), 5, 1);
    balls[++i] = new Ball(830, 245, pow(2, 0.5), pow(2, 0.5), 5, 1);
    balls[++i] = new Ball(830, 275, pow(2, 0.5), pow(2, 0.5), 5, 1);
    balls[++i] = new Ball(830, 305, pow(2, 0.5), pow(2, 0.5), 5, 1);
    balls[++i] = new Ball(860, 230, pow(2, 0.5), pow(2, 0.5), 5, 1);
    balls[++i] = new Ball(860, 260, pow(2, 0.5), pow(2, 0.5), 5, 1);
    balls[++i] = new Ball(860, 290, pow(2, 0.5), pow(2, 0.5), 5, 1);
    balls[++i] = new Ball(860, 320, pow(2, 0.5), pow(2, 0.5), 5, 1);
    balls[++i] = new Ball(945, 285, pow(2, 0.5), pow(2, 0.5), 5, 7);
    balls[++i] = new Ball(740, 285, pow(2, 0.5), pow(2, 0.5), 5, 6);
    balls[++i] = new Ball(535, 285, pow(2, 0.5), pow(2, 0.5), 5, 5);
    balls[++i] = new Ball(235, 285, pow(2, 0.5), pow(2, 0.5), 5, 4);
    balls[++i] = new Ball(235, 367, pow(2, 0.5), pow(2, 0.5), 5, 3);
    balls[++i] = new Ball(235, 203, pow(2, 0.5), pow(2, 0.5), 5, 2);
    balls[i] = new Ball(276, 285, pow(2, 0.5), pow(2, 0.5), 5, 0);
    }

